package teamprojekt.crawler.controller.io.reader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import teamprojekt.crawler.controller.generator.SqlStatementGenerator;
import teamprojekt.crawler.controller.generator.SqlStatementGeneratorImpl;
import teamprojekt.crawler.model.SqlStatement;
import teamprojekt.crawler.model.SqlStatementCache;
import teamprojekt.crawler.model.SqlStatementDefaultImpl;
import teamprojekt.crawler.model.SqlType;

public class HtmlParser2 implements HtmlParser {

	SqlStatementCache cache = new SqlStatementCache();
	SqlStatementGenerator generator = new SqlStatementGeneratorImpl();
	long TEAM_INITIAL = 27257;
	private static int eventID = 1;
	
	@Override
	public SqlStatementCache parseEvents(String url) throws IOException {
		
		Document doc;
		try {
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			throw e;
		}

		Elements table = doc.select("table");
		Elements rows = table.select("tr");
		Elements data = rows.select("td");

		for (int i = 0; i < data.size(); i++) {
			Element link = data.get(i).select("a").first();
			parseTeams("https://ctftime.org" + link.attr("href"));
			List<String> eventData = new ArrayList<String>();
			eventData.clear();
			eventData.add("2017_" + eventID);
			eventID++;

			for (int j = 0; j < 7; j++) {
				if(!(data.get(i + j).text()).equals(""))
				{
					eventData.add(data.get(i + j).text());
				}
				else
				{
					eventData.add("null");
				}
			}
			
			SqlStatement statement = generator.generateSqlStatement(eventData, SqlType.EVENT);
			cache.addStatement(statement);
			i = i + 6;
		}
		return cache;
	}

	private void parseTeams(String url) {

		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Document doc;

		try {
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		Elements table = doc.select("table");
		Elements rows = table.select("tr");
		Elements data = rows.select("td");
		
		if (data.size() > 0) {

			for (int i = 0; i < data.size(); i++) {
				List<String> teamData = new ArrayList<String>();
				TEAM_INITIAL++;
				teamData.add(TEAM_INITIAL + ".0E0");
				teamData.add(data.get(i + 2).text());
				SqlStatement statement = generator.generateSqlStatement(teamData, SqlType.TEAM);
				cache.addStatement(statement);
				i = i + 4;
			}
		} else {
			System.out.println("No ScoreBoard!");
		}

	}
}
