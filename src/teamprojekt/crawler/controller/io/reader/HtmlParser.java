package teamprojekt.crawler.controller.io.reader;

import java.io.IOException;
import java.net.URL;

import teamprojekt.crawler.model.SqlStatement;
import teamprojekt.crawler.model.SqlStatementCache;

/**
 * Interface f�r HtmlParser.
 * 
 * @author Vale
 *
 */
public interface HtmlParser
{

	SqlStatementCache parseEvents(String url) throws IOException;
	
	
}
