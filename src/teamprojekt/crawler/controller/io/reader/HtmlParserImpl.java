package teamprojekt.crawler.controller.io.reader;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;
import com.sun.prism.impl.ps.CachingEllipseRep;

import teamprojekt.crawler.controller.generator.SqlStatementGenerator;
import teamprojekt.crawler.controller.generator.SqlStatementGeneratorImpl;
import teamprojekt.crawler.model.SqlStatement;
import teamprojekt.crawler.model.SqlStatementCache;
import teamprojekt.crawler.model.SqlType;

/**
 * Implementierung von {@link HtmlParser}.
 * 
 * Methode parseEvents() parsed eine Seite mit Event Daten. Daten werden intern an SQL-Generator übergeben.
 * 
 * @author Vale
 *
 */
public class HtmlParserImpl implements HtmlParser {
	
	SqlStatementGenerator sqlGenerator = new SqlStatementGeneratorImpl();
	
	SqlStatementCache cache = new SqlStatementCache();
	static List<SqlStatement> cacheListe = new ArrayList<SqlStatement>();

	@Override
	public SqlStatementCache parseEvents(String eventTableUrl) throws IOException {
		
		cacheListe.clear();
		cacheListe = cache.getList();
		Document doc;
		
		try {
			doc = Jsoup.connect(eventTableUrl).get();
		} catch (IOException e) {
			throw e;
		}
			
			Elements table = doc.select("table");
			Elements rows = table.select("tr");
			Elements data = rows.select("td");

			List<String> eventData = new ArrayList<String>();

			int elementCounter = 1;
			
			for (int i = 0; i < data.size(); i++)
			{
//				System.out.println(data.get(i));
				if (elementCounter <= 7)
				{
					String value = extractData(data.get(i).toString(), elementCounter);
					eventData.add(value.trim());
					elementCounter++;
				}
				else
				{
					elementCounter = 1;
					SqlStatement statement = sqlGenerator.generateSqlStatement(eventData, SqlType.EVENT);
					
					cacheListe.add(statement);
					
//					System.out.println("statement: " + statement.getStatementParams());
					
//					System.out.println("Statement hinzugefügt: " + statement.getStatementParams() + " größe: " + cacheListe.size());
				
					eventData.clear();
					i--;
				}
			}

			for(int j = 0; j < cacheListe.size(); j++)
			{
				System.out.println("bla "+ cacheListe.get(j).getStatementParams() );
			}
			
		cache.setList(cacheListe);
			
		return cache;
	}

	
	/**
	 * Extrahiert reine Daten aus den <td> - Tags. Unterscheidet hier nach Block (1-7) und nach Link oder kein Link.
	 * 
	 * @param input
	 * @param count
	 * @return Reine Daten aus den Zwischenräumen der Tags.
	 */
	private String extractData(String input, int count) {
		String result = null;

		if (count != 1)
		{
			if(!(input.contains("href")))
			{
				Pattern p = Pattern.compile("(\\<.*\\>)(.*)(\\<.*\\>)");
				Matcher m = p.matcher(input);
				m.matches();
				m.groupCount();
				result = m.group(2);
			}
			else
			{
				Pattern p = Pattern.compile("(\\<.*\\>)(\\<.*\\>)(.*)(\\<.*\\>)(\\<.*\\>)");
				Matcher m = p.matcher(input);
				m.matches();
				m.groupCount();
				result = m.group(3);
			}
		}
		else
		{
				Pattern p = Pattern.compile("(\\<.*\\>)(\\<.*\\>)(.*)(\\<.*\\>)(\\<.*\\>)");
				Matcher m = p.matcher(input);
				m.matches();
				m.groupCount();
				result = m.group(3);
		}
		return result;
	}
}
