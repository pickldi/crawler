package teamprojekt.crawler.controller.io.writer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import teamprojekt.crawler.model.SqlStatementCache;
import teamprojekt.crawler.model.SqlType;

public class SqlWriterImpl implements SqlWriter
{
	SqlType type;
	List<String> parameter;
	File file;
	FileWriter writer;
	List<String> results = null;
	
	@Override
	public void writeSql(SqlStatementCache cache)
	{
		results = new ArrayList<String>();
		System.out.println("cache in writer: " + cache.getList().size());
		
		for (int i = 0; i < cache.getList().size(); i++) {
			type = cache.getList().get(i).getSqlType();
			parameter = cache.getList().get(i).getStatementParams();
			switch (type) {
			case EVENT:
				writeStatement("EVENT(ID, EVENTNAME, VON, BIS, VID, MAXPOINTS, BEMERKUNG, ORTID)");
				break;
			case TEAM:
				writeStatement("TEAM(ID, TEAMNAME)");
				break;
			default :
			System.out.println("default");
			}
		}
		
		file = new File("./generated-sources/Statements.txt");
		if(!file.exists())
		{
			try
			{
				file.createNewFile();
				System.out.println("Created new Output File.");
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		try
		{
			writer= new FileWriter(file, true);
			for(String s : results)
			{
				writer.write(s + "\n");
			}
			writer.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void writeStatement(String type){
		String result = "INSERT INTO "+ type +" VALUES (";
		for (int i = 0; i < parameter.size(); i++) {
			if (i == parameter.size()-1) {
				result = result + "'" + parameter.get(i) +"'";
			} else {
				result = result + "'" + parameter.get(i)+"',";
			}
		}
		result = result + ");";
		results.add(result);
	}
}
