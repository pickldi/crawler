package teamprojekt.crawler.controller.io.writer;

import teamprojekt.crawler.model.SqlStatementCache;

/**
 * Reiner Textwriter
 * Bekommt Liste von Strings und schreibt Eintr�ge untereinander in eine .txt Datei.
 * AblageOrt der .txt Datei ist der generated-sources Folder.
 * 
 * @author Vale
 *
 */
public interface SqlWriter
{
	
	void writeSql(SqlStatementCache statements);
	
}
