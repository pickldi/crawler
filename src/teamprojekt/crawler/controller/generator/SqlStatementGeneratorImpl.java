package teamprojekt.crawler.controller.generator;

import java.util.ArrayList;
import java.util.List;

import teamprojekt.crawler.model.SqlStatement;
import teamprojekt.crawler.model.SqlStatementCache;
import teamprojekt.crawler.model.SqlStatementDefaultImpl;
import teamprojekt.crawler.model.SqlType;

public class SqlStatementGeneratorImpl implements SqlStatementGenerator
{
	@Override
	public SqlStatement generateSqlStatement(List<String> eventData, SqlType type)
	{
		List<String> newContentData = null;

		if (type == SqlType.EVENT)
		{
			String time = eventData.get(2);

			String[] fromTo = time.split("\\�");

			newContentData = new ArrayList();

			newContentData.add(eventData.get(0));
			newContentData.add(eventData.get(1));
			newContentData.add(fromTo[0].trim());
			newContentData.add(fromTo[1].trim());
			newContentData.add("130.0E0");
			newContentData.add("null");
			if (!(eventData.get(4).equals("On-line")))
			{
				newContentData.add("" + eventData.get(3) + "; " + eventData.get(4));
				newContentData.add("1");
			}
			else
			{
				newContentData.add(eventData.get(3));
				newContentData.add("1");
			}

			SqlStatement s = new SqlStatementDefaultImpl();
			s.setStatementParams(newContentData);
			s.setSqlType(type);
			return s;
		}
		else
		{
			SqlStatement s = new SqlStatementDefaultImpl();
			s.setStatementParams(eventData);
			s.setSqlType(type);
			return s;
		}
	}
}
