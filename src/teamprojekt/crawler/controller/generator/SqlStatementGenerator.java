package teamprojekt.crawler.controller.generator;

import java.util.List;

import teamprojekt.crawler.model.SqlStatement;
import teamprojekt.crawler.model.SqlType;

/**
 * 
 * Interface f�r Gernerator Klasse zu SQLStatements.
 * Klasse erstellt Statement Objekte und weist diesen Objekten einen Typ zu.
 * 
 *  - 
 * 
 * 
 * @author Vale
 *
 */
public interface SqlStatementGenerator
{

	SqlStatement generateSqlStatement(List<String> eventList, SqlType type);
	
}
