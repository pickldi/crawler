package teamprojekt.crawler.application;

import java.io.IOException;

import teamprojekt.crawler.controller.io.reader.HtmlParser;
import teamprojekt.crawler.controller.io.reader.HtmlParser2;
import teamprojekt.crawler.controller.io.reader.HtmlParserImpl;
import teamprojekt.crawler.controller.io.writer.SqlWriter;
import teamprojekt.crawler.controller.io.writer.SqlWriterImpl;
import teamprojekt.crawler.model.SqlStatement;
import teamprojekt.crawler.model.SqlStatementCache;

public class Application
{
	
	public static void main(String[] args)
	{
		HtmlParser parser = new HtmlParser2();
		SqlStatementCache cache = new SqlStatementCache();
		SqlWriter writer = new SqlWriterImpl();
		try {
			cache = parser.parseEvents("https://ctftime.org/event"
					+ "/list/?year=2017&online=-1&format=0&restrictions=-1&archive=true");	
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		writer.writeSql(cache);	
	}
}
