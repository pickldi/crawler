package teamprojekt.crawler.model;

import java.util.List;

/**
 * Interface f�r SQL-Statement Objekte. Jedes SQL-Statement hat einen {@link SqlType}.
 * 
 * @author Vale
 *
 */
public interface SqlStatement
{
	SqlType getSqlType();
	
	List<String> getStatementParams();
	
	void setSqlType(SqlType sqlType);

	void setStatementParams(List<String> eventData);
}
