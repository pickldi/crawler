package teamprojekt.crawler.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Cache Objekt welches {@link SqlStatement}s enth�lt.
 * 
 * @author Vale
 *
 */
public class SqlStatementCache
{
	
	static List<SqlStatement> sqlStatements = new ArrayList<SqlStatement>();
	
	//getter - setter
	public List<SqlStatement> getList(){
		return SqlStatementCache.sqlStatements;
	}
	
	public void addStatement(SqlStatement statement){
		SqlStatementCache.sqlStatements.add(statement);
	}
	
	public void setList(List<SqlStatement> list){
		sqlStatements = list;
	}
	
	
}
