package teamprojekt.crawler.model;

import java.util.List;

/**
 * SqlStatement Default Objekt.
 * Je nach Typ sind hier unterschiedliche Parameter Enthalten.
 * Beispiel Team: INSERT INTO TEAM(ID, TEAMNAME) VALUES (16706.0E0, '#bb0000#666666');
 * --> Benötigte Felder sind String ID und String TEAMNAME.
 * --> Parameter werden in Liste gespeichert. 
 * 
 * @author Vale
 *
 */
public class SqlStatementDefaultImpl implements SqlStatement
{
	SqlType sqlType;

	List<String> statementParameters;
	
	@Override
	public SqlType getSqlType()
	{
		return this.sqlType;
	}

	@Override
	public List<String> getStatementParams()
	{
		return this.statementParameters;
	}
	
	public void setStatementParams(List<String> list)
	{
		statementParameters = list;
	}

	@Override
	public void setSqlType(SqlType sqlType)
	{
		this.sqlType = sqlType;
	}
}
